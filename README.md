# phy213_multimedia


## Name
Physics 213 Multimedia Final Project (Units Conversion and concepts)

## Description
The idea is that this program should be able to take different types of units commonly found in physics problems, convert those to si units, and apply them to a given physics concept.

## Usage
for example, if a user has units in nanocoulombs and centimeters, and they want to find the force between two charges, our program should convert the units to coulombs,
and return the value for the force vector needed.

## Support
Inquiries can go directly to jjb24@pdx.edu

## Roadmap
Potential for multiple classes in the future to allow for more physics concepts 

## Authors and acknowledgment
Joshua Brown, Joanna Rollot, Harshiv Mistry

