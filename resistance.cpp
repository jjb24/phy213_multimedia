#include "multimedia.h"

float units::resistance(int circuit)
{
	//variable initialization
	int n = 0;	//variable for keeping track of exponent
	float result = 0;	//final result
	int comp = 0; 	//number of capacitors in the circuit
	float resist = 0.0;	//capacitance of a given capacitor
	float volt = 0.0;
	float amp = 0.0;

	if(circuit == 2)	//circuit in parallel
	{
		cout << "How many resistors are in your circuit: ";
		cin >> comp;
		float resistors[comp];  //array to organize resistors

		cout << "----Resistance in Parallel Calculator----\n";
		for(int i = 0; i < comp; i++)
		{
			cout << "Resistance of resistor " << i+1 << ": ";
			cin >> resist;

			units_menu();
			cout << "Prefix for: Resistance of resistor " << i+1 << ": ";
			n = conversion(); // just returns correct conversion power
			resist = resist * pow(10,n); // direct unit conversion using appropriate power
			cout << "Value of Resistor (Ω)" << resist << endl;
		
			resistors[i] = 1/resist; //the reciprocal of the resistance  at resistor i 	
		}

		result = accumulate(resistors, resistors+comp, result);
		cout << "Resistance (Ω) = " << 1/result << "\n";
	}
	else if(circuit == 1)	//circuit in series
	{
		cout << "How many resistors are in your circuit: ";
                cin >> comp;
                float resistors[comp];  //array to organize resistors

		cout << "----Resistance in Series Calculator----\n";
                for(int i = 0; i < comp; i++)
                {
			cout << "Resistance of resistor " << i+1 << ": ";
                        cin >> resistors[i];

                        units_menu();
			cout << "Prefix for: Resistance of resistor " << i+1 << ": ";
                        n = conversion(); // just returns correct conversion power
                        resistors[i] = resistors[i] * pow(10,n); // direct unit conversion using 
								   // appropriate power
                }

                result = accumulate(resistors, resistors+comp, result);
		cout << "Resistance (Ω) = " << result << "\n";
	}
	else
        {
                cout << "----Resistance in a single Resistor----\n";
                cout << "Net Current of the Resistor: ";
                cin >> amp;
                units_menu();
                cout << "Prefix for Current of the Resistor: ";
                n = conversion(); // just returns correct conversion power
                amp = amp * pow(10,n); // direct unit conversion using appropriate power

                cout << "\nNet Voltage of the Resistor: ";
                cin >> volt;
                units_menu();
                cout << "\nPrefix for net Voltage of the Resistor: ";
                n = conversion(); // just returns correct conversion power
                volt = volt * pow(10,n); // direct unit conversion using appropriate power

                result = volt/amp;
                cout << "Resistance (Ω) = " << result << endl;
        }

	return result;
}
