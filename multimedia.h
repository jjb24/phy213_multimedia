// .h file for our Units conversion program
// Joshua, Joanna, Harshiv

#include <iostream>
#include <cstring>
#include <cmath> //allows us to use pow() function
#include <numeric>	//allows us to use accumulate() function
using namespace std;

// Global space function headers
//int units_menu();

class units
{
	// non-recursive functions, wrappers, menu functions	
	public:
		int units_menu(); //just a menu with prefixes
		float concept_menu(); 
		int conversion(); //returns n. In concept function, multiply by 10ⁿ
		
		float capacitance(int circuit); //calculate capacitance in series or parallel
		float resistance(int circuit);	//calculate resistance in series or parallel
		float current();	// I = (V/R)
		float drift_velocity();		// Vd = I/(n*q*(pi*r^2))		
		float voltage();	// V = IR
		float point_chg();		
	private:

};


