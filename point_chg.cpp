#include "multimedia.h"

float units::point_chg()
{
	cout << "----Point Charge Calculator----\n"
	     << "Result given by the equation:\n\n"
	     << "k(q1*q2)\n_______\n\n   r²\n\n";
		

	float q1,q2,r {0.0};

	int n = 0; // variable for keeping track of exponent.
	units_menu();

	cout << "Value for:\nq1: ";
	cin >> q1;
	cin.ignore(100,'\n');
	n = conversion(); // just returns correct conversion power
	q1 = q1 * pow(10,n); // direct unit conversion using appropriate power

	cout << "q2: ";
	cin >> q2;
	cin.ignore(100,'\n');
	n = conversion();
	q2 = q2 * pow(10,n);

	cout << "r: ";
	cin >> r;
	cin.ignore(100,'\n');
	n = conversion();
	r = r * pow(10,n);

	float result = (8.99 * pow(10,9)) * ( (q1 * q2) / pow(r,2) ); // literally just coulomb's law
	cout << "The force on q1 by q2 is: " << result << " N"
	     << "\nThis is given by K (q1*q2) / r²\n"; 
	/*
	 * helpful note: superscript can be typed using ctrl+k, then "num"+S (S is capital) 
	*/

return result;
}

		
