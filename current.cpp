// This file contains the functions and code needed to make the concepts related to current & voltage work. 
// Harshiv, Joanna, Josh 
// Code in this file written by: Harshiv :)

#include "multimedia.h"

//This function will return current (I) in Ampheres, and takes in voltage in (v) and resistance (ohms).
float units::current()
{
	//Local variables
	float voltage {0};
	float resistance {0};
	float result {0};
	int exp {0};

	//Welcome message & equation
	cout    << "\n-----Current Calculator------\n"
		<< "Equation: "
		<< "\n V \n" 
		<< "---\n" 
		<< " R \n";

	units_menu();
	//Getting inputs
	cout    << "\nVoltage (V): ";
	cin     >> voltage;
	cin.ignore(100, '\n');
	exp = conversion();
	voltage = voltage * pow(10,exp);

	cout    << "\nResistance (R): ";
	cin     >> resistance;
	cin.ignore(100, '\n');
	exp = conversion();
	resistance = resistance * pow(10,exp);

	//Confirmation
	cout    << "\nThis is what we got: \n"
		<< "Voltage: " << voltage << "V" << endl
		<< "Resistance: " << resistance << "ohms" <<endl
		<< "\nWith these inputs, after calculating... ";

	//Calculations
	result = (voltage/resistance);
	//Displaying Results
	cout    << "\n\nAnswer: " << result << " A." << endl << endl;
	return 0.0;
}

//This function will return drift velocity in a wire. 
//Takes in current in Ampheres, free charge density, charge in (C), and radius of the wire in meters.
float units::drift_velocity()
{
	//Local variables
	float current {0};
	float free_charge_density{0};
	float charge {0};
	float radius {0};
	float result {0};
	int exp {0}; 

	//Welcome message & equation
	cout    << "\n-----Drift Velocity Calculator------\n"
		<< "Equation: "
		<< "\n     I     \n" 
		<< "--------------\n" 
		<< "q * n * (pi * (r^2))\n";

	units_menu();
	//Getting input
	cout    << "\nCurrent (I): ";
	cin     >> current;
	cin.ignore(100, '\n');
	exp = conversion();
	current = current * pow(10,exp);

	cout    << "\nCharge (q): ";
	cin     >> charge;
	cin.ignore(100, '\n');
	exp = conversion();
	charge = charge * pow(10,exp);

	cout    << "\nFree Charge Density (n): ";
	cin     >> free_charge_density;
	cin.ignore(100, '\n');

	cout    << "\nRadius (r): ";
	cin     >> radius;
	cin.ignore(100, '\n');
	exp = conversion();
	radius = radius * pow(10,exp);

	//Confirmation
	cout    << "\nThis is what we got: \n"
		<< "Current: " << current << "A" << endl
		<< "Charge: " << charge << "C" <<endl
		<< "Free Charge Density : " << free_charge_density << "C/m^2"<< endl
		<< "Radius: " << radius << "m" << endl
		<< "\nWith these inputs, after calculating... ";
	//Calculations
	result = (current/(free_charge_density * charge * (M_PI * (powf32(radius, 2)))));
	//Display results
	cout    << "\n\nAnswer: " << result << " m/s." << endl << endl;
	return 0.0;
}

//This functions takes current and resistance and calculates voltage. 
float units::voltage()
{
	//Local variables
	float current {0};
	float resistance {0};
	float result {0};
	int exp {0};

	//Welcome message & equation
	cout    << "\n-----Voltage Calculator------\n"
		<< "Equation: "
		<< "\n I * R \n";

	units_menu();
	//Getting inputs
	cout    << "\nCurrent (A): ";
	cin     >> current;
	cin.ignore(100, '\n');
	exp = conversion();
	current = current * pow(10,exp);

	cout    << "\nResistance (R): ";
	cin     >> resistance;
	cin.ignore(100, '\n');
	exp = conversion();
	resistance = resistance * pow(10,exp);

	//Confirmation
	cout    << "\nThis is what we got: \n"
		<< "Current: " << current << "A" << endl
		<< "Resistance: " << resistance << "ohms" <<endl
		<< "\nWith these inputs, after calculating... ";

	//Calculations
	result = (current * resistance);
	//Displaying Results
	cout    << "\n\nAnswer: " << result << " V." << endl << endl;
	return 0.0;
}
