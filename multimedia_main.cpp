#include "multimedia.h"
// File housing the main function as well as menu functions
// Joshua Brown, Joanna Rollot, Harshiv Mistry

int main()
{
	units object;
	
	bool again = true;
	char inp = ' ';
	while(again) // while again is true program will loop
	{
		object.concept_menu();
		cout << "Another concept? Input (y/n): ";       //* sure function returns correctly
		cin >> inp;
		cout << "\n";
		if(inp == 'n')
			again = false;	
	}

	return 0;
}


