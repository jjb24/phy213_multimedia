#include "multimedia.h"

//* Menu functions (just testing right now)
int units::units_menu()
{
	cout << "Unit prefixes (input a number from list)\n" 
		<< "(1): nano (n)\n"
		<< "(2): micro (μ)\n"
		<< "(3): milli (m)\n"
		<< "(4): centi (c)\n"
		<< "(5): deci (d)\n"
		<< "(6): deca (da)\n"
		<< "(7): hecto (h)\n"
		<< "(8): kilo (k)\n";

	return 0;
}
//conversion function returns n to multiply ex: "xⁿ"
int units::conversion()
{
	int result = 0;
	int n = 0;
	char need_convert = ' ';
	cout << "Do you need conversion to SI units?(y/n): ";
	cin >> need_convert;
	if(need_convert == 'y')
	{
		cout << "Enter current unit prefix: ";
		cin >> result;
		cout << "\n";

		switch(result) { // we can use these to directly convert in a concept function
			case 1: 
				n = -9; //nano (n)
				break;
			case 2:
				n = -6; //micro (u)
				break;
			case 3: 
				n = -3; //milli (m)
				break;
			case 4:
				n = -2; //centi(c)
				break;
			case 5:
				n = -1; //deci (d)
				break;
			case 6: 
				n = 1; //deca (da)
				break;
			case 7:
				n = 2; //hecto (h)
				break;
			case 8: 
				n = 3; //kilo (k)
				break;
		}	
	}else cout << "Ok!\n";

	return n;
}

// Function to determine what type of physics problem user has
float units::concept_menu()
{       // this could be a list of concepts to choose from. Using pnt. charge for testing.
	cout << "Which physics concept would you like to explore?:\n"
		<< "(1): Force of a point charge on another.\n"
		<< "(2): Current\n"
		<< "(3): Drift velocity\n"
		<< "(4): Voltage\n"
		<< "(5): Capacitance in a Series or a Parallel Circuit.\n"
		<< "(6): Resistance in a Series or a Parallel Circuit.\n"
		<< "(7): TBD\n"
		<< "(8): More on Each Topic.\n"
		<< "Your input: ";	     
	int input = 0;
	cin >> input;

	switch(input) { // getting values since we know the concept
		case 1:
		{
			point_chg(); // calls specific concept function which calls conversion function
			break;
		}
		case 2:
		{
			current();
			break;
		}
		case 3:
		{
			drift_velocity();
			break;
		}
		case 4:
		{
			voltage();
			break;
		}
		case 5:
		{
			int circuit = 0;
			cout << "Find capacitance in (1)Series, in (2)Parallel, or "
				<<	"(3)for a single capacitor: ";
			cin >> circuit;
			capacitance(circuit);

			break;
		}
		case 6:
		{
			int circuit = 0;
			cout << "Find resistance in (1)Series, in (2)Parallel, or "
				<< "(3)for a single resistor: ";
			cin >> circuit;
			resistance(circuit);

			break;
		}
		case 7:
		{
			//empty case
			//please use to call functions and update the menu cout statements to match your function
			break;
		}
		case 8:
		{
			cout << "(1) Coulomb's law"
			     << "\nCoulomb's law states that the force of attraction or repulsion "
			     << "\nbetween two charged bodies is directly proportional to the product "
			     << "\nof their charges and inversely proportional to the square of the "
			     << "\ndistance between them. The direction of the force points along the "
			     << "\nline connecting the two charged objects."
		     	     << "\n\n(2) Current"
			     << "\nCurrent is defined as the rate at which electrons flow past a point "
			     << "\nin a complete electrical circuit. It can be found by dividing "
			     << "\nvoltage by resistance. Current is measured in Ampere where 1A = 1C/s." 
			     << "\n\n(3) Drift Velocity"
			     << "\nDrift Velocity is the average velocity attained by charged particles"
			     << "\nin a material due to an electric field. It can be found by dividing "
			     << "\nthe net current by the product of free electron density, the charge "
			     << "\nof an electron, and the cross sectional area of the material. Drift "
			     << "\nvelocity is measured in m/s."
			     << "\n\n(4) Voltage"
			     << "\nVoltage is the electric potential difference or the change in "
			     << "\nelectric potential. It can be found by multiplying current and "
			     << "\nresistance. Voltage is measured in Volts where 1V = 1J/C."
			     << "\n\n(5) Capacitance"
			     << "\nCapacitance is the physical attribute that describes electrical  "
			     << "\nenergy stored in an object. It can be found by taking the ratio of "
			     << "\nthe maximum charge that can stored in a capacitor to the applied "
			     << "\nvoltage across its plates. Capcitance is measured in Farads where "
			     << "\n1F = 1C/V."
			     << "\n\n(6) Resistance"
			     << "\nResistance is the measure of the opposition to current flow in an "
			     << "\nelectrical circuit. It can be found by dividing voltage by current. "
			     << "\nResistance is measured in Ohms where 1Ω = 1V/A."
			     << "\n\n(7) Series vs Parallel"
			     << "\nIn a series circuit there is only one path for current to follow. "
			     << "\nAdditionally, charge remains the same throughout the circuit. "
			     << "\nHowever, voltage is distributed across each component in the system. "
			     << "\nThe net voltage can therefore be found by adding the voltage of "
			     << "\neach individual component."
			     << "\nIn a parallel circuit current is forced to branch at every new "
			     << "\npath it meets. The net current can therefore be found by adding "
			     << "\nthe current from each divergent branch. Additionally, charge "
			     << "\ndiffers across the system and follows the same properties. "
                             << "\nHowever, voltage remains the same across each component "
			     << "\nin the system. "                             
			     << "\nThese behaviors impact how units such as capacitance or "
			     << "\nresistance are measured in a circuit meaning that different "
			     << "\nformulas must be used to find the correct measurement."
			     << endl;
			break;
		}
	}
	return 0;
}

