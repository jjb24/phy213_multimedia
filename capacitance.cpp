#include "multimedia.h"

float units::capacitance(int circuit)
{
	//variable initialization
	int n = 0;	//variable for keeping track of exponent
	float result = 0;	//final result
	int comp = 0; 	//number of capacitors in the circuit
	float cap = 0.0;	//capacitance of a given capacitor
	float volt = 0;
	float q = 0;

	if(circuit == 1)	//circuit in series
	{
		cout << "Number of capacitors in the circuit: ";
		cin >> comp;

		float capacitors[comp];	//array to organize capacitors
		
		cout << "----Capacitance in Series Calculator----\n";
		for(int i = 0; i < comp; i++)
		{
			cout << "Capacitance of the Capacitor " << i+1 << ": ";
			cin >> cap;

			units_menu();
			cout << "Prefix for Capacitance of the Capacitor " << i+1 << ": ";
			n = conversion(); // just returns correct conversion power
			cap = cap * pow(10,n); // direct unit conversion using appropriate power
		
			capacitors[i] = 1/cap;	//the reciprocal of the capacitance at capacitor i 	
		}

		result = accumulate(capacitors, capacitors+comp, result);
		cout << "Capacitance (F) = " << 1/result << "\n";
	}
	else if (circuit == 2)	//circuit in parallel
	{
		cout << "Number of capacitors in the circuit: ";
		cin >> comp;

		float capacitors[comp];	//array to organize capacitors
		
		cout << "----Capacitance in Parallel Calculator----\n";
                for(int i = 0; i < comp; i++)
                {
                        cout << "Capacitance of the Capacitor " << i+1 << ": ";
                        cin >> capacitors[i];

                        units_menu();
                        cout << "Prefix for Capacitance of the Capacitor " << i+1 << ": ";
                        n = conversion(); // just returns correct conversion power
                        capacitors[i] = capacitors[i] * pow(10,n); // direct unit conversion using 
								   // appropriate power
                }

                result = accumulate(capacitors, capacitors+comp, result);
                cout << "Capacitance (F) = " << result << endl;
	}
	else
	{
		cout << "----Capacitance in a single Capacitor----\n";
		cout << "Net charge of the Capacitor: ";
		cin >> q;
		units_menu();
		cout << "Prefix for Charge of the Capacitor: ";
		n = conversion(); // just returns correct conversion power
		q = q * pow(10,n); // direct unit conversion using appropriate power

		cout << "\nNet Voltage of the Capacitor: ";
		cin >> volt;
		units_menu();
		cout << "\nPrefix for net Voltage of the Capacitor: ";
		n = conversion(); // just returns correct conversion power
		volt = volt * pow(10,n); // direct unit conversion using appropriate power

		result = q/volt;
		cout << "Capacitance (F) = " << result << endl;
	}

	return result;
}
